﻿using UnityEngine;
using System.Collections;

public class AIController : PlayerBase 
{
    private float previousAnswerTime;
    private float timeToNextAnswer = 1f;

    public override void Init(GameManager _manager, int _playerID)
    {
        multiplier = new Multiplier();

        manager = _manager;
        playerID = _playerID;

        previousAnswerTime = Time.time;
    }

    public override void UpdatePlayer()
    {
        if(Time.time - previousAnswerTime > timeToNextAnswer)
        {
            bool answer = GetRandomAnswer();
            Debug.Log(answer);
            float newScore = multiplier.UpdateMultiplier(answer, timeToNextAnswer);
            manager.ProcessAnswer(playerID, answer, timeToNextAnswer, newScore);

            timeToNextAnswer = GetNextAnswerTime(answer);
        }
    }

	void Start () 
    {
	
	}

	void Update () 
    {
	
	}

    private bool GetRandomAnswer()
    {
        return Random.Range(0f, 1f) >= 0.15f ? true : false;
    }

    private float GetNextAnswerTime(bool _result)
    {
        if(_result)
        {
            return Time.time + Random.Range(0.25f, 0.5f);
        }
        else
        {
            return Time.time + Random.Range(0, 0.25f);
        }
    }
}
