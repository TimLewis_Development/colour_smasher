﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public abstract class PlayerBase 
{
    public class AnswerInfo
    {
        public int givenAnswer;
        public int correctAnswer;
        public float answerTime;
    }

    protected GameManager manager;
    protected int playerID;

    protected Multiplier multiplier;
    protected float score;

    protected PaletteManager paletteManager;

    protected List<AnswerInfo> answerInfo;

    public virtual void Init(GameManager _manager, int _playerID)
    {
        manager = _manager;

        multiplier = new Multiplier();

        GameObject paletteCanvas = GameObject.Instantiate(Resources.Load("Prefabs/PaletteCanvas") as GameObject, Vector3.zero, Quaternion.identity) as GameObject;
        paletteManager = paletteCanvas.GetComponent<PaletteManager>();
        paletteManager.Init(this);
    }

    public virtual void UpdatePlayer()
    {
        paletteManager.UpdatePaletteManager();
    }

    public virtual void ProcessAnswer(bool _result, float _answerTime, int _givenAnswer, int _correctAnswer)
    {
        float newScore = multiplier.UpdateMultiplier(_result, _answerTime);
        manager.ProcessAnswer(playerID, _result, _answerTime, newScore);
    }

    public virtual IEnumerator GenerateExtraData(Action success)
    {
        if(answerInfo == null)
        {
            answerInfo = new List<AnswerInfo>();
        }

        yield return null;
    }
}
