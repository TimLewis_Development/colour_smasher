﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{
	public GameObject profilePopup;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void StartInfinite()
	{

	}

	public void StartTimed()
	{

	}

	public void StartBatle()
	{

	}

	public void OpenProfile()
	{
		profilePopup.SetActive(true);
	}

	public void QuitApp()
	{
		Application.Quit();
	}
}
