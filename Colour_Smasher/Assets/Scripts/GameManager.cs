﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour 
{
    public enum GameState
    {
        init,
        running,
        gameOver
    }

    public class PlayerInfo
    {
        public PlayerBase controller;
        public string name;
        public Color primaryColor;
        public Color secondaryColor;
        public float score;
    }

    public GameType.GameMode gameMode;
    public GameObject battleBarCanvas;

    public List<PlayerInfo> playerInfo;

    public GameState gameState;
    private GameType.Base gameType;

    public const int NUM_OF_PLAYERS = 2;
    public float TOTAL_POINTS = 1000;

	void Start () 
    {
        InitPlayers();
	}

	void Update () 
    {
        switch(gameState)
        {
            case GameState.init:
                InitGameType();
                break;
            case GameState.running:
                if(gameType.IsGameOver())
                {
                    SwitchState(GameState.gameOver);
                }
                else
                {
                    for(int i = 0; i < playerInfo.Count; i++)
                    {
                        playerInfo[i].controller.UpdatePlayer();
                    }

                    gameType.DoUpdate();
                }
                break;
            case GameState.gameOver:
                gameType.DoGameOver();
                break;
        }
	}

    private void InitPlayers()
    {
        playerInfo = new List<PlayerInfo>();

        for(int i = 0; i < NUM_OF_PLAYERS; i++)
        {
            PlayerInfo p = new PlayerInfo();

            if(i == 0)
            {
                p.controller = new PlayerController();
                p.controller.Init(this, i);
            }
            else
            {
                p.controller = new AIController();
                p.controller.Init(this, i);
            }

            p.name = "player " + (i+1).ToString();
            p.primaryColor = Color.red;
            p.secondaryColor = Color.blue;
            p.score = TOTAL_POINTS / 2;

            playerInfo.Add(p);
        }
    }

    public void SwitchState(GameState newstate)
    {
        CleanupState(gameState);
        gameState = newstate;
        InitState(gameState);
    }

    private void InitState(GameState state)
    {
        switch(state)
        {
            case GameState.init:
                break;
            case GameState.running:
                break;
            case GameState.gameOver:
                break;
        }
    }

    private void CleanupState(GameState state)
    {
        switch(state)
        {
            case GameState.init:
                break;
            case GameState.running:
                break;
            case GameState.gameOver:
                break;
        }
    }

    public void InitGameType()
    {
        switch(gameMode)
        {
            case GameType.GameMode.infinite:
                gameType = gameObject.AddComponent<GameType.Infinite>();
                break;
            case GameType.GameMode.timeTrial:
                gameType = gameObject.AddComponent<GameType.TimeTrial>();
                break;
            case GameType.GameMode.battle:
                gameType = gameObject.AddComponent<GameType.Battle>();
                break;
        }

        gameType.Init();

        SwitchState(GameState.running);
    }

    public void ProcessAnswer(int _playerID, bool _result, float _answerTime, float _newScore)
    {
        gameType.ProcessAnswer(_playerID, _result, _answerTime, _newScore);
    }
}
