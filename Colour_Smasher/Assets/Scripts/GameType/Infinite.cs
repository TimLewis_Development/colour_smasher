﻿using UnityEngine;
using System.Collections;

namespace GameType
{
    public class Infinite : Base 
    {
        public override void Init()
        {
            base.Init();
        }

        public override void DoUpdate()
        {
        }

        public override bool IsGameOver()
        {
            return false;
        }

        public override void DoGameOver()
        {
        }

        public override void ProcessAnswer(int _playerID, bool _result, float _answerTime, float _newScore)
        {
        }
    }
}