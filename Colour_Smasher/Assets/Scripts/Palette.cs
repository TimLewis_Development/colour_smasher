﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Palette : MonoBehaviour
{
	PaletteManager manager;

	public List<GameObject> colorButtons;
	public GameObject answerObject;

	public bool markedForDeath = false;
	private bool lerping = false;
	private float lerpTimer = 0f;
	private float lerpInterval = 0.2f;
	public Vector3 lerpTarget = Vector3.zero;

	private Coroutine lerpCoroutine;
	private List<Color> buttonColors;

	// Use this for initialization
	public void Init(PaletteManager _manager)
	{
		manager = _manager;
		lerpTarget = transform.localPosition;

		buttonColors = new List<Color>{Color.red, Color.green, Color.blue, Color.yellow};
	}

	private IEnumerator LerpToTarget(bool _killAtEnd)
	{
		if(_killAtEnd)
		{
			manager.RemoveAnsweredPalette();
			markedForDeath = true;
		}

		Vector3 startPos = transform.localPosition;

		lerping = true;

		while(lerpTimer < lerpInterval)
		{
			lerpTimer += Time.deltaTime;

			float percent = lerpTimer / lerpInterval;

			Vector3 newPos = Vector3.Lerp(startPos, lerpTarget, percent);
			transform.localPosition = newPos;

			yield return null;
		}

		transform.localPosition = lerpTarget;
		lerping = false;

		if(_killAtEnd)
		{
//			manager.RemoveAnsweredPalette();
			Destroy(gameObject);
		}
	}

	public void SetLerpTarget(Vector3 _lerpTarget, bool _killAtEnd)
	{
		lerpTarget = _lerpTarget;
		lerpTimer = 0;

		if(lerpCoroutine != null)
		{
			StopCoroutine(lerpCoroutine);
		}

		lerpCoroutine = StartCoroutine(LerpToTarget(_killAtEnd));
	}

	public void AnswerSelected(int position)
	{
		manager.AnswerSelected(position);
	}

	private void SetButtonColor(int buttonIndex, int colorIndex)
	{
		colorButtons[buttonIndex].GetComponent<Image>().color = buttonColors[colorIndex];
	}

	public void SetButtonColors(List<int> colors)
	{
		for (int i = 0; i < colorButtons.Count; i++)
		{
			SetButtonColor(i, colors[i]);
		}
	}

	public void SetAnswerColor(int colorIndex)
	{
		answerObject.GetComponent<Image>().color = buttonColors[colorIndex];
	}
}
