﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BattleBar_Controller : MonoBehaviour 
{
    public RectTransform underlayBar;
    public RectTransform overlayBar;

    private float maxBarSize;
    private float pointsPerUnit;
    private float totalWidthValue;

	void Awake () 
    {
        maxBarSize = underlayBar.sizeDelta.x;
    }
	
	void Update () 
    {
        
	}

    public void InitScores(float _totalWidthValue, float _startPercentage)
    {
        totalWidthValue = _totalWidthValue;
        pointsPerUnit = maxBarSize / totalWidthValue;

        overlayBar.sizeDelta = new Vector2((totalWidthValue * _startPercentage) * pointsPerUnit, overlayBar.sizeDelta.y);
    }

    /// <summary>
    /// Updates the bar scores. Takes in the second player score as thats the one that's actually changed.
    /// </summary>
    /// <param name="player2Score">Player2 score.</param>
    public void UpdateOverlayBar(float _overlayBarPercentage)
    {
        overlayBar.sizeDelta = new Vector2((totalWidthValue *_overlayBarPercentage) * pointsPerUnit, overlayBar.sizeDelta.y);
    }
}