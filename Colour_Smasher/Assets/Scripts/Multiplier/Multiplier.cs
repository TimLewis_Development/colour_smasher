﻿using UnityEngine;
using System.Collections;

public class Multiplier 
{
    private float currentScoreAddition = 5f;
    private float answerTimeThreshold = 2f;
    private int mulitplier;

    private const float STARTING_ANSWER_THRESHOLD = 2f;
    private const float STARTING_SCORE_ADDITION = 5f;

    public float UpdateMultiplier(bool _result, float _answerTime)
    {
        if(_result && _answerTime <= answerTimeThreshold)
        {
            IncrementMultiplier();
        }
        else
        {
            ResetMultiplier();
        }

        return currentScoreAddition;
    }

    private void ResetMultiplier()
    {
        answerTimeThreshold = STARTING_ANSWER_THRESHOLD;
        currentScoreAddition = STARTING_SCORE_ADDITION;
        mulitplier = 0;
    }

    private void IncrementMultiplier()
    {
        mulitplier++;
        currentScoreAddition = STARTING_SCORE_ADDITION * mulitplier;
    }
}
