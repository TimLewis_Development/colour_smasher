﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PaletteManager : MonoBehaviour
{
    private PlayerBase playerController;

	public List<PaletteRow> palettes;

	private int currentRow = 0;
	private GameObject palettePrefab;

	private const int levelToStartShuffling = 10;
	private const int maxPossibleAnswer = 4;

	[System.Serializable]
	public class PaletteRow
	{
		public GameObject palletObject;
		public int correctAnswer;
		public List<int> colorIndices;
	}

	// Use this for initialization
	void Start ()
	{
	}

    public void Init(PlayerBase _playerController)
    {
        palettes = new List<PaletteRow>();

        playerController = _playerController;

        palettePrefab = (GameObject)Resources.Load("Prefabs/Palette");

        float offsetY = -240;
        // Create 4 starting rows
        for( int i = 0; i < 4; i++ )
        {
            palettes.Add(new PaletteRow());

            palettes[i].palletObject = (GameObject)Instantiate(palettePrefab);
            palettes[i].palletObject.transform.SetParent(transform);

            palettes[i].palletObject.transform.localScale = Vector3.one;

            Vector3 position = new Vector3(0, offsetY, 0);
            palettes[i].palletObject.transform.localPosition = position;

            palettes[i].palletObject.GetComponent<Palette>().Init(this);

            palettes[i].colorIndices = new List<int>{0, 1, 2, 3};
            palettes[i].palletObject.GetComponent<Palette>().SetButtonColors(palettes[i].colorIndices);

            palettes[i].correctAnswer = Random.Range(0, maxPossibleAnswer);
            //          palettes[i].correctAnswer = 0;
            palettes[i].palletObject.GetComponent<Palette>().SetAnswerColor(palettes[i].correctAnswer);

            offsetY += 140;
        }
    }

	private void GenerateNewRow()
	{
		// Instantiate new palette
		palettes.Add(new PaletteRow());

		palettes[palettes.Count - 1].palletObject = (GameObject)Instantiate(palettePrefab);
		palettes[palettes.Count - 1].palletObject.transform.SetParent(transform);

		// TODO: Set a const for highest position.
		Vector3 position = new Vector3(0, 320, 0);
		palettes[palettes.Count - 1].palletObject.transform.localPosition = position;

		palettes[palettes.Count - 1].palletObject.GetComponent<Palette>().Init(this);

		palettes[palettes.Count - 1].colorIndices = new List<int>{0, 1, 2, 3};
		// Generate the colors for the new row
		GenerateColorIndices();

		palettes[palettes.Count - 1].palletObject.GetComponent<Palette>().SetButtonColors(palettes[palettes.Count - 1].colorIndices);

		palettes[palettes.Count - 1].correctAnswer = Random.Range(0, maxPossibleAnswer);
//		palettes[palettes.Count - 1].correctAnswer = 0;
		palettes[palettes.Count - 1].palletObject.GetComponent<Palette>().SetAnswerColor(palettes[palettes.Count - 1].correctAnswer);

		//TODO: Lerp all rows to new positions.
		List<Vector3> previousPos = new List<Vector3>(){new Vector3(0, -500, 0),new Vector3(0, -220, 0),new Vector3(0, -80, 0),new Vector3(0, 60, 0),new Vector3(0, 200, 0)};

		for( int i = palettes.Count - 1; i >= 0; i-- )
		{
//			Vector3 targetPos = palettes[i].palletObject.transform.localPosition;
			Vector3 targetPos = previousPos[i];
			//TODO: Use the button height
//			targetPos.y -= 140;

			//TODO: Make the lerping not reset the current position.
			palettes[i].palletObject.GetComponent<Palette>().SetLerpTarget(new Vector3(targetPos.x, targetPos.y, targetPos.z), (i == 0));
		}
	}

	private void GenerateColorIndices()
	{
		if(currentRow > 10)
		{
			palettes[palettes.Count - 1].colorIndices = new List<int>{0, 1, 2, 3};

			for (int i = 0; i < 100; i++)
			{
				int firstIndex = Random.Range(0, maxPossibleAnswer);
				int secondIndex = Random.Range(0, maxPossibleAnswer);

				int whileCounter = 0;
				int maxWhileLoop = 20;

				while(firstIndex == secondIndex)
				{
					secondIndex = Random.Range(0, maxPossibleAnswer);

					whileCounter++;

					if(whileCounter > maxWhileLoop)
					{
						if(firstIndex == maxPossibleAnswer - 1)
						{
							secondIndex = 0;
						}
						else
						{
							secondIndex++;
						}
					}
				}

				int tempIndex = palettes[palettes.Count - 1].colorIndices[firstIndex];
				palettes[palettes.Count - 1].colorIndices[firstIndex] = palettes[palettes.Count - 1].colorIndices[secondIndex];
				palettes[palettes.Count - 1].colorIndices[secondIndex] = tempIndex;
			}
		}
	}
	
    public void UpdatePaletteManager()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = new Ray(Camera.main.transform.position, Camera.main.ScreenToViewportPoint(Input.mousePosition));
            RaycastHit hit;

            Debug.DrawRay(Camera.main.transform.position, Camera.main.ScreenToViewportPoint(Input.mousePosition), Color.red, 1000f);

            if(Physics.Raycast(ray, out hit, 100f ))
            {
                Debug.LogWarning("Hit: " + hit.collider.name);
            }
        }

        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            AnswerSelected(0);
        }

        if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            AnswerSelected(1);
        }

        if(Input.GetKeyDown(KeyCode.Alpha3))
        {
            AnswerSelected(2);
        }

        if(Input.GetKeyDown(KeyCode.Alpha4))
        {
            AnswerSelected(3);
        }
    }

	public void RemoveAnsweredPalette()
	{
		palettes.RemoveAt(0);
	}

	public void AnswerSelected(int indexSelected)
	{
		if(palettes[currentRow].colorIndices[indexSelected] == palettes[currentRow].correctAnswer)
		{
			//TODO: Correct answer
			GenerateNewRow();
            playerController.ProcessAnswer(true, 0, indexSelected, palettes[currentRow].correctAnswer);
		}
		else
		{
			//TODO: Incorrect answer
		}
	}
}
